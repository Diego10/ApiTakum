let express = require('express'),
  app = express(),
  port = process.env.PORT || 8001;

var bodyParser = require('body-parser');
let mysql = require('mysql');


app.use(bodyParser.json());
app.listen(port);
//app.set('json spaces', 40);
//console.log('todo list RESTful API server started on: ' + port + ', yay!');

//let index = require('./api/routes/index');
let users = require('./api/routes/users');
let products = require('./api/routes/products');
let catgs = require('./api/routes/categories');


app.use(function(req, res, next){
	global.connection = mysql.createConnection({
		host : 'localhost',
		user : 'root',
		password : '',
		database : 'db_takum'
	});
	connection.connect();
	next();
});
//app.use('/', index);
app.use('/users', users);
app.use('/products', products);
app.use('/categorias', catgs);