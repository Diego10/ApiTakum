DROP DATABASE IF EXISTS db_takum;
CREATE DATABASE db_takum;
USE db_takum;

DROP TABLE IF EXISTS USUARIO;
DROP TABLE IF EXISTS ROL;
DROP TABLE IF EXISTS PRODUCTO;


CREATE TABLE ROL (
    id                      INT NOT NULL AUTO_INCREMENT,
    descripcion             VARCHAR(250)  NOT NULL,
    PRIMARY KEY ( id )
);
INSERT INTO ROL(`descripcion`) VALUES ("Rol A");
INSERT INTO ROL(`descripcion`) VALUES ("Rol B");

CREATE TABLE USUARIO (
    id                          INT NOT NULL AUTO_INCREMENT,
    nombre                      VARCHAR(50) NOT NULL,
    nickname                    VARCHAR(15) NOT NULL UNIQUE,
    contrasena                  VARCHAR(20) NOT NULL,
    rol_id                      INT NOT NULL,
    PRIMARY KEY ( id ),
    FOREIGN KEY (rol_id) REFERENCES ROL(id)
);
INSERT INTO USUARIO(`nombre`, `nickname`, `contrasena`, `rol_id`) VALUES ("Diego Zuniga","diego10","america",1);
INSERT INTO USUARIO(`nombre`, `nickname`, `contrasena`, `rol_id`) VALUES ("Juana Lozano","juanalo","123",2);

CREATE TABLE CATEGORIA (
    id                          INT NOT NULL AUTO_INCREMENT,
    nombre                      VARCHAR(50) NOT NULL,
    PRIMARY KEY ( id )
);
INSERT INTO CATEGORIA(`nombre`) VALUES ("Camisas");
INSERT INTO CATEGORIA(`nombre`) VALUES ("Pantalones");
INSERT INTO CATEGORIA(`nombre`) VALUES ("Zapatos");
INSERT INTO CATEGORIA(`nombre`) VALUES ("Sombreros");
INSERT INTO CATEGORIA(`nombre`) VALUES ("Otros");

CREATE TABLE PRODUCTO (
    id                          INT NOT NULL AUTO_INCREMENT,
    nombre                      VARCHAR(50) NOT NULL,
    descripcion                 VARCHAR(250) NOT NULL,
    costo                       INT(10) NOT NULL,
    rol_id                      INT NOT NULL,
    catg_id                      INT NOT NULL,
    PRIMARY KEY ( id ),
    FOREIGN KEY (rol_id) REFERENCES ROL(id),
    FOREIGN KEY (catg_id) REFERENCES CATEGORIA(id)
);
INSERT INTO PRODUCTO(`nombre`, `descripcion`, `costo`, `rol_id`, `catg_id`) VALUES ("Camiseta Cuello en V","Descripcion de la Camiseta Cuello en V",35000,1,1);
INSERT INTO PRODUCTO(`nombre`, `descripcion`, `costo`, `rol_id`, `catg_id`) VALUES ("Pantalon Gris","descripcion del Pantalon Gris",90000,1,2);
INSERT INTO PRODUCTO(`nombre`, `descripcion`, `costo`, `rol_id`, `catg_id`) VALUES ("Sombrero Negro","Descripcion del Sombrero Negro",50000,1,4);
INSERT INTO PRODUCTO(`nombre`, `descripcion`, `costo`, `rol_id`, `catg_id`) VALUES ("Gorra Azul","Descripcion de la Gorra Azul",25000,1,4);
INSERT INTO PRODUCTO(`nombre`, `descripcion`, `costo`, `rol_id`, `catg_id`) VALUES ("Correa","Descripcion de la Correa",50000,1,5);


INSERT INTO PRODUCTO(`nombre`, `descripcion`, `costo`, `rol_id`, `catg_id`) VALUES ("Buso Manga Larga","Descripcion del Buso Manga Larga",35000,2,1);
INSERT INTO PRODUCTO(`nombre`, `descripcion`, `costo`, `rol_id`, `catg_id`) VALUES ("Zapatos Deportivos","descripcion de los Zapatos Deportivos",90000,2,3);
INSERT INTO PRODUCTO(`nombre`, `descripcion`, `costo`, `rol_id`, `catg_id`) VALUES ("Pantaloneta de Baño","descripcion de los Pantaloneta de Baño",90000,2,2);