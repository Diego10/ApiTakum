# ApiTakum

## Development server

First, install dependencies with `npm install`, then run `npm start` for a dev server. Navigate to `http://localhost:8001/`. The app will automatically reload if you change any of the source files.


## SQL

The `db.sql` located in this path works with MySql.


## Test Users
The following users can be used for test purposes:

User 1
- **Username:** diego10
- **Password:** america

User 2
 - **Username:** juanlo
 - **Password:** 123

