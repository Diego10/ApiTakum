var express = require('express');
var atob = require('atob');
var router = express.Router();

//Consultar Categorias
router.get('/', function(req, res, next) {
	let headers_user = {
		username: atob( req.headers.authorization.split(" ")[1] ).split(":")[0],
		password: atob( req.headers.authorization.split(" ")[1] ).split(":")[1]
	}

	//Primero se comprueba si el usuario existe.
	sql_user_exist = "SELECT * from USUARIO WHERE nickname = '" + headers_user.username + "'";
	connection.query(sql_user_exist, function (error, results, fields) {
	  	if(error){
	  		res.status(500).send(null); 
	  	} else {
	  		if (results.length == 0) {
	  			res.status(401).send("Nombre de Usuario y/o contraseña incorrectos.");
	  		}
	  		else{
	  			//Luego se valida que la contrasena sea la correcta.
	  			if (headers_user.password != results[0].contrasena) {
	  				res.status(401).send("Nombre de Usuario y/o contraseña incorrectos.");
	  			}
	  			else{
					let get_sql = "SELECT * FROM CATEGORIA";

					connection.query(get_sql, function (error, results, fields) {
				  		if(error){
				  			res.status(500).send(null); 
					  	} else {
							res.send(results);
					  	}
					});
	  			}
	  		}
	  	}
  	});
});

module.exports = router;