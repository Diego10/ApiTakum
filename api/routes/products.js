var express = require('express');
var atob = require('atob');
var router = express.Router();

//Lista de Productos
router.get('/', function(req, res, next) {

	let headers_user = {
		username: atob( req.headers.authorization.split(" ")[1] ).split(":")[0],
		password: atob( req.headers.authorization.split(" ")[1] ).split(":")[1]
	}

	//Se obtienen todos los productos del rol que tiene ese usuario.
	let sql = 	"SELECT PROD.id, PROD.nombre, PROD.descripcion, PROD.costo, CATEGORIA.nombre as categoria FROM PRODUCTO AS PROD " +
				"JOIN USUARIO ON PROD.rol_id = USUARIO.rol_id " +
				"JOIN CATEGORIA ON PROD.catg_id = CATEGORIA.id " +
				"WHERE USUARIO.nickname = '" + headers_user.username + "' " + 
				"ORDER BY CATEGORIA.nombre";

	connection.query(sql, function (error, results, fields) {
	  	if(error){
	  		res.status(500).send(null); 
	  	} else {
	  		let grouped_products = {};
	  		for (var i in results){

	  			//grouped_products[ results[i].categoria ] = 0;
	  			if (grouped_products.hasOwnProperty( results[i].categoria )) {
	  				grouped_products[ results[i].categoria ].push(results[i]);
	  			}
	  			else{
	  				grouped_products[ results[i].categoria ] = [results[i]]
	  			}
	  		}
  			res.status(200).send(grouped_products);
	  	}
  	});
});

//Crear un Producto
router.post('/', function(req, res, next) {
	let headers_user = {
		username: atob( req.headers.authorization.split(" ")[1] ).split(":")[0],
		password: atob( req.headers.authorization.split(" ")[1] ).split(":")[1]
	}
	
	let sql = "SELECT rol_id FROM USUARIO WHERE nickname='" + headers_user.username + "'";
	
	connection.query(sql, function (error, results, fields) {
	  	if(error){
	  		res.send({"status": 500, "error": error, "response": null});
	  	} else {
	  		//res.status(200).send(req.body); 

	  		let rol_id = parseInt( results[0].rol_id );

  			let create_sql = "INSERT INTO PRODUCTO(`nombre`, `descripcion`, `costo`, `rol_id`, `catg_id`) " +
  							 "VALUES ('" + req.body.nombre + "', '" + req.body.descripcion + "', " + req.body.costo + "," + rol_id + "," + req.body.catg_id + ")";
			
			connection.query(create_sql, function (error, results, fields) {
			  	if(error){
			  		res.status(500).send(null); 
			  	} else {
		  			res.status(200).send({ id: results.insertId, message: "Producto creado satisfactoriamente."});
			  	}
		  	});
		  	
	  	}
  	});
});

//Eliminar un producto
router.delete('/:product_id', function(req, res, next) {
	let headers_user = {
		username: atob( req.headers.authorization.split(" ")[1] ).split(":")[0],
		password: atob( req.headers.authorization.split(" ")[1] ).split(":")[1]
	}

	//Primero se comprueba si el usuario existe.
	sql_user_exist = "SELECT * from USUARIO WHERE nickname = '" + headers_user.username + "'";
	connection.query(sql_user_exist, function (error, results, fields) {
	  	if(error){
	  		res.status(500).send(null); 
	  	} else {
	  		if (results.length == 0) {
	  			res.status(401).send("Nombre de Usuario y/o contraseña incorrectos.");
	  		}
	  		else{
	  			//Luego se valida que la contrasena sea la correcta.
	  			if (headers_user.password != results[0].contrasena) {
	  				res.status(401).send("Nombre de Usuario y/o contraseña incorrectos.");
	  			}
	  			else{
					let delete_sql = "DELETE FROM PRODUCTO WHERE id='" + req.param("product_id") + "'";

					connection.query(delete_sql, function (error, results, fields) {
				  		if(error){
	  						res.status(500).send(null); 
					  	} else {
							res.status(200).send({message: "Producto creado satisfactoriamente :D."});
					  	}
					});
	  			}
	  		}
	  	}
  	});
});


//Consultar un Producto
router.get('/:product_id', function(req, res, next) {

	let sql = "SELECT * FROM PRODUCTO WHERE id='" + req.param("product_id") + "'";

	connection.query(sql, function (error, results, fields) {
	  	if(error){
			res.status(500).send(null);
	  	} else {
  			res.status(200).send(results[0]);
	  	}
  	});
});


module.exports = router;